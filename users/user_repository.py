from db import get_db
from users.user import User

class UserRepository:
    def get_all_users(self):      
        # db = get_db()
        # cursor = db.cursor()
        # print('Getting all users')
        # cursor.execute('SELECT * FROM users')
        # users_data = cursor.fetchall()
        # users = [User(user_data[0], user_data[1], user_data[2]) for user_data in users_data]
        # return users;
        with get_db() as db:
            with db.cursor() as cursor:
                print('Getting all users')
                cursor.execute('SELECT * FROM users')
                users_data = cursor.fetchall()
                users = [User(user_data[0], user_data[1], user_data[2]) for user_data in users_data]
                return users;
    
    def get_user_by_id(self, id):        
        db = get_db()
        cursor = db.cursor()
        print('Getting user by id')
        cursor.execute('SELECT * FROM users WHERE id = %s', (id,))
        user_data = cursor.fetchone()
        if user_data is None:
            return None
        return User(user_data[0], user_data[1], user_data[2])
 