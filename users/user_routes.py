from flask import Blueprint, jsonify
from users.user_repository import UserRepository

users_blueprint = Blueprint('users', __name__)

# TODO inject UserRepository

@users_blueprint.route('/users')
def get_all_users():
    user_repository = UserRepository()
    users = user_repository.get_all_users()
    return jsonify([user.__dict__ for user in users])

@users_blueprint.route('/users/<int:id>')
def get_user_by_id(id):
    user_repository = UserRepository()
    user = user_repository.get_user_by_id(id)
    if user is None:
        return 'User not found', 404
    return jsonify(user.__dict__)

@users_blueprint.route('/users/test')
def test():
    print('test')
    user_repository = UserRepository()
    users = user_repository.get_all_users()
    users = user_repository.get_all_users()
    users = user_repository.get_all_users()
    user = user_repository.get_user_by_id(1)
    return 'test';    