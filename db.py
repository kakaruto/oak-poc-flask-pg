from flask import g, current_app
import psycopg2


def get_db():
    if 'db' not in g:
        print('Connecting to database')
        g.db = psycopg2.connect(
            host = current_app.config['DATABASE_HOST'],
            port = current_app.config['DATABASE_PORT'],
            database = current_app.config['DATABASE_NAME'],
            user = current_app.config['DATABASE_USER'],
            password = current_app.config['DATABASE_PASSWORD']
        )        
    return g.db

def close_db():
    db = g.pop('db', None)
    if db is not None:
        print('Closing database connection')
        db.close()