from flask import Flask
from db import close_db

app = Flask(__name__)

# TODO move to .env
app.config['DATABASE_HOST'] = '127.0.0.1'
app.config['DATABASE_PORT'] = '5432'
app.config['DATABASE_NAME'] = 'oak'
app.config['DATABASE_USER'] = 'postgres'
app.config['DATABASE_PASSWORD'] = 'mysecretpassword'

@app.after_request
def close_db_test(response):
    close_db()
    return response


from users.user_routes import users_blueprint
app.register_blueprint(users_blueprint)

if __name__ == '__main__':
    app.run()